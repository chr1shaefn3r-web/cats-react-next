import styles from "./Cat.module.css";
import { AliveCat } from "./CatTiles";
import Inside from "./Inside";
import Outside from "./Outside";

export default function Cat({ name, inside, toggleCat }: AliveCat) {
    const boundToggleCat = toggleCat.bind(undefined, inside);
    return (
        <div className={styles.cat}>
            {inside && <Inside name={name} toggleCat={boundToggleCat} />}
            {!inside && <Outside name={name} toggleCat={boundToggleCat} />}
        </div>
    );
}
