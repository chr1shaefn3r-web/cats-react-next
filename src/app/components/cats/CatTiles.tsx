'use client';

import { collection, doc, query, updateDoc } from 'firebase/firestore';
import { useFirestore, useFirestoreCollectionData } from 'reactfire';
import Cat from "./Cat";
import styles from "./CatTiles.module.css";
import DeceasedCat from "./DeceasedCat";
import LoadingCat from './LoadingCat';

export interface DeceasedCat extends NamedCat, OrderedCat {
    deceased: string;
}

export interface ToggleableCat extends NamedCat {
    toggleCat: any;
}

export interface AliveCat extends NamedCat, OrderedCat, ToggleableCat {
    inside: boolean;
}
export interface NamedCat {
    name: string;
}

interface OrderedCat {
    order: number;
}

export type Cat = NamedCat & OrderedCat & Partial<AliveCat> & Partial<DeceasedCat>;

export interface CatTiles {
}

export default function CatTiles(_props: CatTiles) {
    const fireStore = useFirestore();
    const catsCollection = collection(fireStore, 'cats');
    const catsQuery = query(catsCollection);
    const { status, data } = useFirestoreCollectionData(catsQuery);

    if (status === "loading") {
        return (<LoadingCat />)
    }

    const cats = data as Cat[];
    const toggleCat = (id: string, currentInsideState: boolean) => {
        const catRef = doc(fireStore, 'cats', id);
        updateDoc(catRef, {
            inside: !currentInsideState,
        });
    }
    return (
        <div className={styles.catTiles}>
            {cats
                .filter(isDeceased)
                .sort(byOrder)
                .map(({ name, deceased: decased, order }) => (
                    <DeceasedCat key={name} name={name} deceased={decased} order={order} />
                ))}
            {cats
                .filter(isAlive)
                .sort(byOrder)
                .map((cat) => (
                    <Cat key={cat.name} {...cat} toggleCat={toggleCat.bind(undefined, (cat as any)["NO_ID_FIELD"] as string)} />
                ))}
        </div>
    );
}

function isAlive(cat: Cat): cat is AliveCat {
    return (cat as AliveCat).inside !== undefined;
}

function isDeceased(cat: Cat): cat is DeceasedCat {
    return (cat as DeceasedCat).deceased !== undefined;
}

function byOrder(a: Cat, b: Cat) {
    return a.order - b.order;
}