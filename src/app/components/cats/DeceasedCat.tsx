import styles from "./DeceasedCat.module.css";
import type { DeceasedCat } from "./CatTiles";
import DeceasedBadge from "../badge/DeceasedBadge";

const catsImage = (name: string, size = "") => `./cats/${name.toLowerCase()}${size}-deceased.jpg`;

export default function DeceasedCat({ name, deceased: decased }: DeceasedCat) {
    return (
        <div className={styles.deceasedCat}>
            <div className={styles.inside}>

                <button>
                    <img src={catsImage(name)} alt={name}
                        srcSet={`${catsImage(name)} 100w, ${catsImage(name, "150")} 150w, ${catsImage(name, "200")} 200w`}
                        sizes="(max-width: 650px) 100px, (max-width: 850px) 150px, 200px" />
                </button>
                <DeceasedBadge text={decased} />
            </div>
        </div>
    );
}
