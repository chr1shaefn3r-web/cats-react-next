import styles from "./LoadingCat.module.css";

export default function LoadingCat() {
    return (
        <div className={styles.loadingCat}>
            <img src="./cat-walking.gif" alt="Loading Cat" />
        </div>
    );
}
