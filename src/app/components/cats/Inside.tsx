import styles from "./Inside.module.css";
import { ToggleableCat } from "./CatTiles";
import InsideBadge from "../badge/InsideBadge";
import { useState } from "react";
import { animationDuration, nearlySeconds } from "./animationConstants";

const catsImage = (name: string, size = "") => `./cats/${name.toLowerCase()}${size}.jpg`;

export default function Inside({ name, toggleCat }: ToggleableCat) {
    const [flip, setFlip] = useState("");

    const onButtonClick = (e: React.MouseEvent<HTMLButtonElement>) => {
        setFlip(styles.flip);
        setTimeout(() => {
            setFlip("");
            toggleCat();
        }, animationDuration * nearlySeconds);
    }

    return (
        <div className={styles.inside}>
            <button className={flip} onClick={onButtonClick}>
                <img src={catsImage(name)} alt={name}
                    srcSet={`${catsImage(name)} 100w, ${catsImage(name, "150")} 150w, ${catsImage(name, "200")} 200w`}
                    sizes="(max-width: 650px) 100px, (max-width: 850px) 150px, 200px" />
            </button>
            <InsideBadge />
        </div>
    );
}
