import styles from "./Outside.module.css";
import { ToggleableCat } from "./CatTiles";
import OutsideBadge from "../badge/OutsideBadge";
import { useState } from "react";
import { animationDuration, nearlySeconds } from "./animationConstants";

export default function Outside({ name, toggleCat }: ToggleableCat) {
    const [flip, setFlip] = useState("");

    const onButtonClick = (e: React.MouseEvent<HTMLButtonElement>) => {
        setFlip(styles.flip);
        setTimeout(() => {
            setFlip("");
            toggleCat();
        }, animationDuration * nearlySeconds);
    }

    return (
        <div className={styles.outside}>
        <button className={flip} onClick={onButtonClick}>{name}</button>
            <OutsideBadge />
        </div>
    );
}
