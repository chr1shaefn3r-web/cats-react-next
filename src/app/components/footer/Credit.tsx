import styles from "./Credit.module.css";

export default function Credit() {
    return (
        <div>
            <p className={styles.credit}>&copy; Christoph Häfner</p>
        </div>
    )
}
