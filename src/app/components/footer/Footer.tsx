import styles from "./Footer.module.css";
import Credit from "./Credit"

export default function Footer() {
  return (
    <div>
        <div className={styles.footer}>
          <Credit />
        </div>
    </div>
  );
}
