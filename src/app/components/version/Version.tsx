import styles from "./Version.module.css";
import GitLink from "./GitLink"

export interface Repo {
    url: string;
    gitHead: string;
}

export interface ProjectVersion {
    time: string;
    deployer: {
        name: string;
        machine: string;
    };
    repo: Repo;
    pipeline: string;
    versionString: string;
}

export default function Version(version: ProjectVersion) {
    return (
        <div className={styles.version}>
            <fieldset>
                <legend>Version Info</legend>
                Build {version.time.substr(0, 10)} by {version.deployer.name} on {version.deployer.machine} from <GitLink {...version.repo} /> &nbsp;via
                &apos;{version.pipeline}&apos; pipeline as v{version.versionString}
            </fieldset>
        </div>
    );
}
