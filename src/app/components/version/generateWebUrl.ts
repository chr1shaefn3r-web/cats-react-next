const generateWebUrl = (gitUrl: string | undefined, gitHead: string | undefined) => {
    if (!gitUrl || !gitHead) {
        return "";
    }
    const webBase = gitUrl
        .replace(":", "/")
        .replace("git+ssh///git@", "https://")
        .replace("git@", "https://")
        .replace(".git", "/");
    return `${webBase}commit/${gitHead}`;
}

export default generateWebUrl
