import generateWebUrl from "./generateWebUrl";
import styles from "./GitLink.module.css";
import { Repo } from "./Version";

export default function GitLink(props: Repo) {
    const webUrl = generateWebUrl(props.url, props.gitHead);
    return (
        <span className={styles.gitlink}>
            <a href={webUrl}>{props.gitHead.substr(0, 6)}</a>
        </span>
    );
}
