import styles from "./Badge.module.css";

export enum BadgeBackgroundColor {
    green = "green",
    red = "red",
    black = "black"
}

export interface Badge {
    text: string;
    backgroundColor: BadgeBackgroundColor;
}

export default function Badge({ text, backgroundColor }: Badge) {
    return (
        <div className={[styles.badge, styles[backgroundColor]].join(" ")}>
            {text}
        </div>
    );
}
