import Badge, { BadgeBackgroundColor } from "./Badge"

export default function DeceasedBadge() {
    return (
        <Badge text={"Drinnen"} backgroundColor={BadgeBackgroundColor.red} />
    )
};