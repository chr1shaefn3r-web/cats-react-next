import Badge, { BadgeBackgroundColor } from "./Badge"

export default function DeceasedBadge() {
    return (
        <Badge text={"Draußen"} backgroundColor={BadgeBackgroundColor.green} />
    )
};