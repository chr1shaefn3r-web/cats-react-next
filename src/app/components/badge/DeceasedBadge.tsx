import Badge, { BadgeBackgroundColor } from "./Badge"

export interface DeceasedBadge {
    text: string;
}

// Latin cross
export default function DeceasedBadge({ text }: DeceasedBadge) {
    return (
        <Badge text={`✝ ${text}`} backgroundColor={BadgeBackgroundColor.black} />
    )
};