import styles from "./LoginForm.module.css";
import LabeledInput from "./LabeledInput";

export interface LoginForm {
    onSubmit: any;
    onChange: any;
}

export default function LoginForm({ onSubmit, onChange }: LoginForm) {
    return (
        <div className={styles.loginForm}>
            <fieldset>
                <legend>Login with eMail and password</legend>

                <LabeledInput text="eMail" name="email" type="text" onChange={onChange} />

                <LabeledInput text="Password" name="password" type="password" onChange={onChange} />

                <input type="submit" value="Sign in" onClick={onSubmit} />

            </fieldset>
        </div>
    );
}
