import styles from "./LabeledInput.module.css";

export interface LabeledInput {
    name: string;
    text: string;
    type: string;
    onChange: any;
}

export default function LabeledInput({name, text, type, onChange}: LabeledInput) {
    return (
        <div className={styles.labeledInput}>
            <input type={type} id={name} name={name} onChange={onChange} placeholder={text}/>
        </div>
    );
}
