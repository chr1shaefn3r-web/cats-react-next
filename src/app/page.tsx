import styles from "./page.module.css";
import App from './app';

export default function Home() {
  return (
    <main className={styles.main}>
      <h1>Katzenstatus</h1>

      <App />
    </main>
  );
}
