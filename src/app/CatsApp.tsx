'use client'

import { getFirestore } from 'firebase/firestore';
import { FirestoreProvider, useFirebaseApp } from 'reactfire';
import Footer from "./components/footer/Footer"
import Version, { ProjectVersion } from "./components/version/Version"
import Login from "./components/login/Login";
import CatTiles, { Cat } from "./components/cats/CatTiles";

const mockVersion: ProjectVersion = {
    time: "",
    deployer: {
        name: "christoph",
        machine: "rechenknecht",
    },
    repo: {
        url: "",
        gitHead: "abcdefghijklmnopqrstuvwxyz"
    },
    pipeline: "prod",
    versionString: "1.7.0"
}

export default function CatsApp() {
    const firestoreInstance = getFirestore(useFirebaseApp());
    return (
        <FirestoreProvider sdk={firestoreInstance}>
            <CatTiles />
            <Login />
            <Version {...mockVersion} />
            <Footer />
        </FirestoreProvider>
    )
}