import type { Metadata } from "next";
import "./globals.css";

export const metadata: Metadata = {
  title: "Katzenstatus",
  description: "A digital version of our \"which cat is in the house\" installation at home.",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body>{children}</body>
    </html>
  );
}
