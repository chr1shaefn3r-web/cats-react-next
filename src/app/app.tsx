'use client'

import { FirebaseAppProvider } from 'reactfire';
import CatsApp from "./CatsApp"

const firebaseConfig = {
    apiKey: "AIzaSyCUmcHjQh0foPT7RUyw_dighGRzak6hzwI",
    authDomain: "cats-react.firebaseapp.com",
    databaseURL: "https://cats-react.firebaseio.com",
    projectId: "cats-react",
    storageBucket: "cats-react.appspot.com",
    messagingSenderId: "478404914390",
    appId: "1:478404914390:web:1d7e561a557e3ea27a2d77"
};

export default function App() {
    return (
        <FirebaseAppProvider firebaseConfig={firebaseConfig}>
            <CatsApp />
        </FirebaseAppProvider>
    )
}