import generateWebUrl from '../../../src/app/components/version/generateWebUrl'

describe('generateWebUrl', () => {
    it('should return an empty string if given nothing', () => {
        const result = generateWebUrl();
        expect(result).toEqual("");
    })
    it('should convert ssh-based git repo url into http-based', () => {
        const initialCommit = "6ed7d6d76424ef3e907dd8164d8e78457ccebda9";
        const result = generateWebUrl("git@gitlab.com:chr1shaefn3r-web/cats-react-next.git", initialCommit);
        expect(result).toEqual("https://gitlab.com/chr1shaefn3r-web/cats-react-next/commit/" + initialCommit);
    })
    it('should convert git+ssh-based git repo url into http-based', () => {
        const initialCommit = "6ed7d6d76424ef3e907dd8164d8e78457ccebda9";
        const result = generateWebUrl("git+ssh://git@gitlab.com/chr1shaefn3r-web/cats-react-next.git", initialCommit);
        expect(result).toEqual("https://gitlab.com/chr1shaefn3r-web/cats-react-next/commit/" + initialCommit);
    })
})
