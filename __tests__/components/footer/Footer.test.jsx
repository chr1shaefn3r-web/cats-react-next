import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
import Footer from '../../../src/app/components/footer/Footer'

describe('Footer', () => {
    it('renders my name', () => {
        render(<Footer />)

        const myName = screen.getByText(/Christoph/)

        expect(myName).toBeInTheDocument()
    })
})
