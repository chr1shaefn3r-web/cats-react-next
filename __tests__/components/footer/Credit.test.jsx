import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
import Credit from '../../../src/app/components/footer/Credit'

describe('Credit', () => {
    it('renders my name', () => {
        render(<Credit />)

        const myName = screen.getByText(/Christoph/)

        expect(myName).toBeInTheDocument()
    })
})
