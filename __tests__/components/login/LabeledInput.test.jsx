import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import LabeledInput from '../../../src/app/components/login/LabeledInput'

describe('LabeledInput', () => {
    const getProps = () => ({
        onChange: jest.fn(),
        text: "My Placeholder"
    })
    it('should render a input with a placeholder', () => {
        const props = getProps()
        render(<LabeledInput {...props}/>)

        const inputWithPlaceholder = screen.getByPlaceholderText(props.text)

        expect(inputWithPlaceholder).toBeInTheDocument()
    })
    it('should call onChange if input changes', async () => {
        const user = userEvent.setup()
        const mockInput = 'Hello, World!'
        const props = getProps()
        render(<LabeledInput {...props}/>)

        const inputWithPlaceholder = screen.getByPlaceholderText(props.text)

        await user.click(inputWithPlaceholder)
        await user.keyboard(mockInput)

        expect(props.onChange).toHaveBeenCalledTimes(mockInput.length)
        expect(props.onChange.mock.calls[0][0].target.value).toBe(mockInput)
    })

})
