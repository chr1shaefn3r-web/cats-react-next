import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import LoginForm from '../../../src/app/components/login/LoginForm'

describe('LoginForm', () => {
    const getProps = () => ({
        onChange: jest.fn(),
        onSubmit: jest.fn(),
    });
    it('should render a input for eMail + Password and a Submit-Button', () => {
        render(<LoginForm {...getProps()}/>)

        const inputEmail = screen.getByPlaceholderText("eMail")
        expect(inputEmail).toBeInTheDocument()
        const inputPassword = screen.getByPlaceholderText("Password")
        expect(inputPassword).toBeInTheDocument()
        const submitButton = screen.getByRole("button", {name: /Sign in/i})
        expect(submitButton).toBeInTheDocument()
    })

    it('should call onSubmit if submit button is pressed', async () => {
        const user = userEvent.setup()
        const props = getProps();
        render(<LoginForm {...props}/>)

        const submitButton = screen.getByRole("button", {name: /Sign in/i})
        await user.click(submitButton)

        expect(props.onSubmit).toHaveBeenCalledTimes(1)
    })

    it('should call onChange when input fields are changed', async () => {
        const user = userEvent.setup()
        const props = getProps();
        const mockInput = "Mock Data"
        render(<LoginForm {...props}/>)

        const inputEmail = screen.getByPlaceholderText("eMail")
        await user.click(inputEmail)
        await user.keyboard(mockInput)

        expect(props.onChange).toHaveBeenCalledTimes(mockInput.length);
        expect(props.onChange.mock.calls[0][0].target.value).toBe(mockInput)
    })
})
